
import sys

from oslo_config import cfg


CLI_OPTS = [
    cfg.BoolOpt('diskusage', default=False, short='d',
               help='Return disk usage for object ring'),

    cfg.IntOpt('dfmin', default='90',
               help='Def min'),

    cfg.ListOpt('mylist', default=['a', 'b'],
               help='Example list'),

    cfg.StrOpt('target', default='paf',
               help='Target to use'),
]

def _prepare_config():
     CONF = cfg.ConfigOpts()
     CONF.register_cli_opts(CLI_OPTS)
     return CONF

def main():
    CONF = _prepare_config()
    CONF(sys.argv[1:])

    # Example on how to use the value:
    if CONF.diskusage:
        print(f'Disk usage in cmd line, --dfmin: {CONF.dfmin} --mylist: {CONF.mylist} --target {CONF.target}')
    else:
        print(f'Disk usage NOT in cmd line, --dfmin: {CONF.dfmin} --mylist: {CONF.mylist} --target {CONF.target}')
